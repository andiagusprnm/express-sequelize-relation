'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    // id
    // user_id
    // amount
    // created_at
    return queryInterface.bulkInsert('wallets', [
      {
        id: 1,
        user_id: 1,
        amount: 1000000,
        created_at: new Date(),
      },
      {
        id: 2,
        user_id: 2,
        amount: 500000,
        created_at: new Date(),
      },
    ],{});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.delete('wallets');
  }
};
