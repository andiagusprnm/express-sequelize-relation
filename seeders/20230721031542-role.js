'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    // id
    // role
    // created_at
    // updated_at
    return queryInterface.bulkInsert('roles', [
      {
        id: 1,
        role: 'admin',
        created_at: new Date(),
      },
      {
        id: 2,
        role: 'member',
        created_at: new Date(),
      }
    ],{});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.delete('roles');
  }
};
