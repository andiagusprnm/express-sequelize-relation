'use strict';
const bcrypt = require('bcrypt');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    // id
    // email
    // password
    // first_name
    // last_name
    // role_id
    await queryInterface.bulkInsert('users', [
      {
        id: 1,
        email: 'rusdi@gmail.com',
        password: await bcrypt.hash('secret', 10),
        first_name: 'Rusdi',
        role_id: 1,
        created_at: new Date(),
      },
      {
        id: 2,
        email: 'anto@gmail.com',
        password: await bcrypt.hash('secret', 10),
        first_name: 'Anto',
        role_id: 2,
        created_at: new Date(),
      },
      {
        id: 3,
        email: 'aldi@gmail.com',
        password: await bcrypt.hash('secret', 10),
        first_name: 'Aldi',
        role_id: 1,
        created_at: new Date(),
      }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.delete('users');
  }
};
