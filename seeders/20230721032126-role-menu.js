'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    // id
    // role_id
    // menu_id
    // created_at
    // updated_at

    return queryInterface.bulkInsert('role_menus', [
      {
        id: 1,
        role_id: 1,
        menu_id: 1,
        created_at: new Date(),
      },
      {
        id: 1,
        role_id: 1,
        menu_id: 2,
        created_at: new Date(),
      },
      {
        id: 1,
        role_id: 1,
        menu_id: 3,
        created_at: new Date(),
      },
      {
        id: 1,
        role_id: 2,
        menu_id: 2,
        created_at: new Date(),
      },
      {
        id: 1,
        role_id: 2,
        menu_id: 3,
        created_at: new Date(),
      },
    ],{});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.delete('role_menus');
  }
};
