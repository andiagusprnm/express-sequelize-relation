'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    // id
    // menu
    // parent_id
    // is_active
    // created_at
    return queryInterface.bulkInsert('menus', [
      {
        id: 1,
        menu: 'admin',
        parent_id: 0,
        is_active: 1,
        created_at: new Date(),
      },
      {
        id: 2,
        menu: 'member',
        parent_id: 0,
        is_active: 1,
        created_at: new Date(),
      },
      {
        id: 3,
        menu: 'account',
        parent_id: 0,
        is_active: 1,
        created_at: new Date(),
      }
    ], {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.delete('menus');
  }
};
