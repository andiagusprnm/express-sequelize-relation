const express = require('express');
const { User, Wallet, Role, Menu } = require('./models');
const app = express();

app.get('/user', async (req, res) => {

  const users = await User.findAll({
    include: [
      {
        model: Wallet,
        as: 'wallet'
      },
      {
        model: Role,
        as: 'role'
      },
    ],
  });
  return res.json({
    status: 200,
    message: 'success',
    data: users
  });
});

app.get('/user/:id', async (req, res) => {

  const users = await User.findOne({
    where: { id: req.params.id },
    include: [
      {
        model: Wallet,
        as: 'wallet'
      },
      {
        model: Role,
        as: 'role',
        include: {
          model: Menu,
          as: 'menus'
        },
      },
    ],
  });
  return res.json({
    status: 200,
    message: 'success',
    data: users
  });
});

app.get('/menus', async (req, res) => {

  const menu = await Menu.findAll({
    include: [
      {
        model: Role,
        as: 'roles'
      },
      // {
      //   model: Role,
      //   as: 'role'
      // },
    ],
  });
  return res.json({
    status: 200,
    message: 'success',
    data: menu
  });
});

app.get('/roles', async (req, res) => {

  const menu = await Role.findAll({
    include: [
      {
        model: Menu,
        as: 'menus'
      },
      // {
      //   model: Role,
      //   as: 'role'
      // },
    ],
  });
  return res.json({
    status: 200,
    message: 'success',
    data: menu
  });
});
app.listen(5000, () => console.log('running'));