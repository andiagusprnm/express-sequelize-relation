'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // has one wallet
      User.hasOne(models.Wallet, { foreignKey: 'user_id', as: 'wallet' });
      // belong to role
      User.belongsTo(models.Role, { foreignKey: 'role_id', as: 'role' });
    }
  }
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.TEXT,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    role_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
    timestamps: false,
  });
  User.tableName = 'users';
  return User;
};