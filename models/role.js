'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // one to many users
      Role.hasMany(models.User, { foreignKey: 'role_id', as: 'user' });
      //many to many menus
      Role.belongsToMany(models.Menu, { foreignKey: 'role_id', through: { model: 'role_menu' }, as: 'menus' });
    }
  }
  Role.init({
    role: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'Role',
    timestamps: false,
  });
  Role.tableName = 'roles';
  return Role;
};