'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Menu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // many to many role
      Menu.belongsToMany(models.Role, { foreignKey: 'menu_id', through: { model: 'role_menu' }, as: 'roles' });
    }
  }
  Menu.init({
    menu: DataTypes.STRING,
    parent_id: DataTypes.INTEGER,
    is_active: DataTypes.SMALLINT,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'Menu',
    timestamps: false,
  });
  Menu.tableName = 'menus';
  return Menu;
};